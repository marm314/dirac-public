The DIRAC program suite buildup results of Miro Ilias
-----------------------------------------------------


CDash diverse testings
~~~~~~~~~~~~~~~~~~~~~~

`DIRAC on testboard.org <https://testboard.org/cdash/index.php?project=DIRACext>`_


The CI workers build status badges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Complete CI buildups are visible only to Miro Ilias, because these CI services 
fetch the DIRAC code from his private gitlab and bitbucket repositories.

* codeship

.. image:: https://codeship.com/projects/f2013800-96a4-0132-fbdf-7a1406c9da98/status?branch=master
       :target: https://codeship.com/projects/63067

* wercker

.. image:: https://app.wercker.com/status/dc65c7af4c58c28259c541fb166e5108/m/master
       :target: https://app.wercker.com/project/bykey/dc65c7af4c58c28259c541fb166e5108

* shippable

.. image:: https://api.shippable.com/projects/5676d56d1895ca447466ad11/badge?branch=master
       :target: https://app.shippable.com/projects/5676d56d1895ca447466ad11

* semaphoreci

.. image:: https://semaphoreci.com/api/v1/projects/b5752e42-3309-4871-a7ef-d96be17c439d/379656/badge.svg
       :target: https://semaphoreci.com/miro_ilias/my-dirac

* bitrise

* nevercode

* codefresh

|Codefresh build status|

.. |Codefresh build status| image:: https://g.codefresh.io/api/badges/build?repoOwner=miro.ilias&repoName=dirac&branch=master&pipelineName=dirac&accountName=miroilias&key=eyJhbGciOiJIUzI1NiJ9.NTk3MDg0MzZmNjdjMGEwMDAxMjc4MTk2.lCkFjZSdnRbGR8dHIjM3lLdo-DEteme2R4ykF_8S39M&type=cf-1
   :target: https://g.codefresh.io/repositories/miro.ilias/dirac/builds?filter=trigger:build;branch:master;service:597088ef26998d0001219c2c~dirac

* bitbucket-pipelines
